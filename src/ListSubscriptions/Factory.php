<?php

namespace Tomplay\ListSubscriptions;


class Factory
{
    public static function create(string $type, Persister $persister): ListSubscriptions
    {
        if ('stripe' === $type) {
            return new Stripe($persister);
        }
        throw new \Exception('Unknown type');
    }

}