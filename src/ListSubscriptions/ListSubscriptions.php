<?php

namespace Tomplay\ListSubscriptions;

interface ListSubscriptions
{
    public function loadSubscriptions(int $limit = 100);
}