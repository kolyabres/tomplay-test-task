<?php

namespace Tomplay\ListSubscriptions;


interface Persister
{

    public function persist(array $data);
}