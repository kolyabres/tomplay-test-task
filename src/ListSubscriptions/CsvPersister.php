<?php

namespace Tomplay\ListSubscriptions;

use League\Csv\Reader;
use League\Csv\Writer;

class CsvPersister implements Persister
{
    private $writer;

    /**
     * CsvPersister constructor.
     */
    public function __construct($path)
    {
        $this->writer = Writer::createFromPath($path, 'w+');
    }

    public function persist(array $data)
    {
        $this->writer->insertAll($data);
    }

    public static function showLastRecords($dataDir)
    {
        $files = scandir($dataDir);
        if (count($files) == 3) {
            return [];
        }
        $lastFile = sprintf("%s/%s", $dataDir, array_pop($files));
        try {
            return iterator_to_array(Reader::createFromPath($lastFile)->getRecords());
        } catch (\Exception $e) {
            return [];
        }
    }
}