<?php

namespace Tomplay\ListSubscriptions;

use Stripe\Stripe as StripeClient;

class Stripe implements ListSubscriptions
{
    /**
     * @var Persister
     */
    private $persister;

    public function __construct(Persister $persister)
    {
        StripeClient::setApiKey(getenv('STRIPE_API_KEY'));
        $this->persister = $persister;
    }

    public function loadSubscriptions(int $limit = 100)
    {
        $startingAfter = null;
        do {
            $response = \Stripe\Subscription::all([
                'limit' => $limit,
                'starting_after' => $startingAfter,
                'status' => 'all'
            ]);
            $startingAfter = $this->processItems($response['data']);
        } while ($response['has_more']);
    }

    private function processItems(array $items)
    {
        $list = [];
        $lastItemId = null;
        foreach ($items as $item) {
            foreach ($item['items']['data'] as $subscriptionItem) {
                try {
                    $subscription = [
                        'customer_id' => $item['customer'],
                        'plan' => $subscriptionItem['plan']['nickname'],
                        'status' => $item['status'],
                        'start_date' => date(DATE_RSS, $subscriptionItem['created']),
                        'expire_date' => date(DATE_RSS, $item['current_period_start']),
                    ];
                    $list[] = $subscription;

                } catch (\Exception $e) {
                    //todo some logos here
                    continue;
                } finally {
                    $lastItemId = $item['id'];
                }
            }
        }
        $this->persister->persist($list);
        return $lastItemId;
    }
}