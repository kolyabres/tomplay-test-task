# About

Application displays list of plans from stripe.

Because of performance reasons decided retrieve subscriptions in separate job that can be run hourly or daily via cron job.

# How to start

 - clone the repository `git clone git@github.com:kolyabres/tomplay.git`
 - cd app directory `cd tomplay`
 - install required dependencies `composer install`
 - add `STRIPE_API_KEY` to the `.env`
  file
 - for getting the subscriptions list run command `php job.php`
 - run `php -S localhost:8000 -t public/`, open [http://localhost:8000]() and enjoy :)
 

# How to add another subscriptions source?

Just add new class that implements `src/ListSubscriptions/ListSubscriptions.php` interface and modify factory here `src/ListSubscriptions/Factory.php`. Do the same for Persister
