<?php
require_once __DIR__ . '/bootstrap.php';

$persister = new \Tomplay\ListSubscriptions\CsvPersister($storagePath . '/' . time() . '.csv');
$subscriptions = \Tomplay\ListSubscriptions\Factory::create('stripe', $persister)->loadSubscriptions(1);