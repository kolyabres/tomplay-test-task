<html>
<head><title>List of subscriptions</title></head>
<link rel="stylesheet" href="/css/bootstrap.min.css">
<body>
<div class="container">
    <h1>List of subscriptions</h1>
    <?php
    require_once '../bootstrap.php';

    const HEADERS = [
        'Customer id',
        'Plan',
        'Status',
        'Created',
        'Expire'
    ];
    $records = \Tomplay\ListSubscriptions\CsvPersister::showLastRecords($storagePath);

    if (!$records) {
        echo '<div class="alert alert-danger" role="alert">No Subscriptions! please run job:<br>

       <pre>php job.php</pre> 
       </div>';
    } else {
        echo (new \League\Csv\HTMLConverter())
            ->table('table table-bordered table-hover')
            ->convert(array_merge([HEADERS], $records));
    }
    ?>
</div>
</body>
</html>